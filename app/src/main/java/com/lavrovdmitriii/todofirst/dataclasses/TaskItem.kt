package com.lavrovdmitriii.todofirst.dataclasses

class TaskItem() {
    var itemId: Long = -1
    var itemToTasksId: Long = -1
    var itemName = ""
    var itemCreatedAt = ""
    var isCompleted = false
}