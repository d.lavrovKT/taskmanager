package com.lavrovdmitriii.todofirst.dataclasses

class Tasks() {
    var id: Long = -1
    var name = ""
    var createdAt = ""
    var items: MutableList<TaskItem> = ArrayList()
}