package com.lavrovdmitriii.todofirst

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.lavrovdmitriii.todofirst.adapters.TasksAdapter
import com.lavrovdmitriii.todofirst.database.DBHandler
import com.lavrovdmitriii.todofirst.databinding.ActivityMainListBinding
import com.lavrovdmitriii.todofirst.databinding.DialogTasksBinding
import com.lavrovdmitriii.todofirst.dataclasses.Tasks

class MainList : AppCompatActivity() {
    lateinit var dbHandler: DBHandler
    private lateinit var binding: ActivityMainListBinding
    private lateinit var bindingDialog: DialogTasksBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dbHandler = DBHandler(this)
        binding = ActivityMainListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarMainList)
        title = "Список задач"
        binding.RWTaskList.layoutManager = LinearLayoutManager(this)

        binding.IWAddBtn.setOnClickListener {
            val dialog = AlertDialog.Builder(this)
            dialog.setTitle("Добавить задачу")
            bindingDialog = DialogTasksBinding.inflate(layoutInflater)
            with(bindingDialog) {
                dialog.setView(bindingDialog.root)
                dialog.setPositiveButton("Добавить") { _: DialogInterface, _: Int ->
                    if (etTasks.text.isNotEmpty()) {
                        val tasks = Tasks()
                        tasks.name = etTasks.text.toString()
                        dbHandler.addToTask(tasks)
                        refreshTaskList()
                    }
                }
                dialog.setNegativeButton("Отмена") { _: DialogInterface, _: Int ->
                }
                dialog.show()
            }
        }
    }

    fun updateTaskList(tasks: Tasks) {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Редактировать задачу")
        bindingDialog = DialogTasksBinding.inflate(layoutInflater)
        with(bindingDialog) {
            bindingDialog.etTasks.setText(tasks.name)
            dialog.setView(bindingDialog.root)
            dialog.setPositiveButton("Обновить") { _: DialogInterface, _: Int ->
                if (etTasks.text.isNotEmpty()) {
                    tasks.name = etTasks.text.toString()
                    dbHandler.updateToTask(tasks)
                    refreshTaskList()
                }
            }
            dialog.setNegativeButton("Отмена") { _: DialogInterface, _: Int ->
            }
            dialog.show()
        }
    }

    override fun onResume() {
        refreshTaskList()
        super.onResume()
    }

    fun refreshTaskList() {
        binding.RWTaskList.adapter = TasksAdapter(this, dbHandler.getToTasks())
    }

}