package com.lavrovdmitriii.todofirst.constants

const val DB_NAME = "TaskManager"
const val DB_VERSION = 1
const val TABLE_TASKS = "Tasks"

const val COL_TASKS_ID = "id"
const val COL_TASKS_NAME = "name"
const val COL_TASKS_CREATED_AT = "createdAd"

const val TABLE_TASK_ITEM = "TaskItem"
const val COL_TASK_ITEM_ID = "TaskId"
const val COL_TASK_ITEM_NAME = "TaskName"
const val COL_TASK_ITEM_CREATED_AT = "TaskCreatedAd"
const val COL_TASK_ITEM_TO_TASKS_ID = "TaskToTaskId"
const val COL_IS_COMPLETED = "ColIsCompleted"

const val INTENT_TODO_ID = "TodoId"
const val INTENT_TODO_NAME = "TodoName"
