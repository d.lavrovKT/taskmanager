package com.lavrovdmitriii.todofirst.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.lavrovdmitriii.todofirst.constants.*
import com.lavrovdmitriii.todofirst.dataclasses.TaskItem
import com.lavrovdmitriii.todofirst.dataclasses.Tasks

class DBHandler(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        val createTasksTable = "CREATE TABLE Tasks (" +
                "$COL_TASKS_ID integer PRIMARY KEY AUTOINCREMENT," +
                "$COL_TASKS_NAME string ," +
                "$COL_TASKS_CREATED_AT datetime DEFAULT CURRENT_TIMESTAMP);"
        val createTaskItem = "CREATE TABLE TaskItem (" +
                "$COL_TASK_ITEM_ID integer PRIMARY KEY AUTOINCREMENT," +
                "$COL_TASK_ITEM_NAME string," +
                "$COL_TASK_ITEM_CREATED_AT datetime DEFAULT CURRENT_TIMESTAMP," +
                "$COL_IS_COMPLETED varchar," +
                "$COL_TASK_ITEM_TO_TASKS_ID integer);"

        db.execSQL(createTasksTable)
        db.execSQL(createTaskItem)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    fun addToTask(tasks: Tasks): Boolean {
        val db: SQLiteDatabase = writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_TASKS_NAME, tasks.name)
        val result: Long = db.insert(TABLE_TASKS, null, contentValues)
        return result != (-1).toLong()
    }

    fun updateToTask(tasks: Tasks) {
        val db: SQLiteDatabase = writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_TASKS_NAME, tasks.name)
        db.update(TABLE_TASKS, contentValues, "$COL_TASKS_ID=?", arrayOf(tasks.id.toString()))
    }

    fun deleteToTask(tasksId: Long) {
        val db = writableDatabase
        db.delete(TABLE_TASK_ITEM, "$COL_TASK_ITEM_TO_TASKS_ID=?", arrayOf(tasksId.toString()))
        db.delete(TABLE_TASKS, "$COL_TASKS_ID=?", arrayOf(tasksId.toString()))
    }

    @SuppressLint("Range")
    fun updateToTaskItemCompletedStatus (tasksId: Long, isCompleted: Boolean) {
        val db = writableDatabase

        val queryResult = db.rawQuery("SELECT * from $TABLE_TASK_ITEM WHERE $COL_TASK_ITEM_TO_TASKS_ID=$tasksId", null)
        if (queryResult.moveToFirst()) {
            do {
                val itemTasks = TaskItem()
                itemTasks.itemId = queryResult.getLong(queryResult.getColumnIndex(COL_TASK_ITEM_ID))
                itemTasks.itemToTasksId = queryResult.getLong(queryResult.getColumnIndex(COL_TASK_ITEM_TO_TASKS_ID))
                itemTasks.itemName = queryResult.getString(queryResult.getColumnIndex(COL_TASK_ITEM_NAME))
                itemTasks.isCompleted = isCompleted
                updateItemTask(itemTasks)
            } while (queryResult.moveToNext())
        }
        queryResult.close()
    }

    @SuppressLint("Range")
    fun getToTasks(): MutableList<Tasks> {
        val result: MutableList<Tasks> = ArrayList()
        val db: SQLiteDatabase = readableDatabase
        val queryResult: Cursor = db.rawQuery("SELECT * from $TABLE_TASKS", null)
        if (queryResult.moveToFirst()) {
            do {
                val tasks = Tasks()
                tasks.id = queryResult.getLong(queryResult.getColumnIndex(COL_TASKS_ID))
                tasks.name = queryResult.getString(queryResult.getColumnIndex(COL_TASKS_NAME))
                result.add(tasks)
            } while (queryResult.moveToNext())
        }

        queryResult.close()
        return result
    }

    fun addToItemTask(taskItem: TaskItem): Boolean {
        val db = writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_TASK_ITEM_NAME, taskItem.itemName)
        contentValues.put(COL_TASK_ITEM_TO_TASKS_ID, taskItem.itemToTasksId)
        if (taskItem.isCompleted) {
            contentValues.put(COL_IS_COMPLETED, 1)
        } else {
            contentValues.put(COL_IS_COMPLETED, 0)
        }
        val result = db.insert(TABLE_TASK_ITEM, null, contentValues)
        return result != (-1).toLong()
    }

    @SuppressLint("Recycle", "Range")
    fun getToItemTask(toTaskId: Long): MutableList<TaskItem> {
        val result = ArrayList<TaskItem>()
        val db = readableDatabase
        val queryResult = db.rawQuery("SELECT * from $TABLE_TASK_ITEM WHERE $COL_TASK_ITEM_TO_TASKS_ID=$toTaskId", null)
        if (queryResult.moveToFirst()) {
            do {
                val itemTasks = TaskItem()
                itemTasks.itemId = queryResult.getLong(queryResult.getColumnIndex(COL_TASK_ITEM_ID))
                itemTasks.itemToTasksId = queryResult.getLong(queryResult.getColumnIndex(COL_TASK_ITEM_TO_TASKS_ID))
                itemTasks.itemName = queryResult.getString(queryResult.getColumnIndex(COL_TASK_ITEM_NAME))
                itemTasks.isCompleted = queryResult.getInt(queryResult.getColumnIndex(COL_IS_COMPLETED)) == 1
                result.add(itemTasks)
            } while (queryResult.moveToNext())
        }

        queryResult.close()
        return result
    }

    fun updateItemTask(taskItem: TaskItem) {
        val db = writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_TASK_ITEM_NAME, taskItem.itemName)
        contentValues.put(COL_TASK_ITEM_TO_TASKS_ID, taskItem.itemToTasksId)
        contentValues.put(COL_IS_COMPLETED, taskItem.isCompleted)

        db.update(TABLE_TASK_ITEM, contentValues, "$COL_TASK_ITEM_ID=?", arrayOf(taskItem.itemId.toString()))
    }

    fun deleteToItemTask(tasksId: Long) {
        val db = writableDatabase
        db.delete(TABLE_TASK_ITEM, "$COL_TASK_ITEM_ID=?", arrayOf(tasksId.toString()))
    }
}
