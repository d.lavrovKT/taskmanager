package com.lavrovdmitriii.todofirst.adapters

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.lavrovdmitriii.todofirst.R
import com.lavrovdmitriii.todofirst.TaskItemActivity
import com.lavrovdmitriii.todofirst.databinding.RvChildTaskItemBinding
import com.lavrovdmitriii.todofirst.dataclasses.TaskItem

class TaskItemAdapter(private val activity: TaskItemActivity, private val list: MutableList<TaskItem>) :
    RecyclerView.Adapter<TaskItemAdapter.TaskItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskItemAdapter.TaskItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rv_child_task_item, parent, false)
        return TaskItemHolder(view)
    }

    @SuppressLint("SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: TaskItemHolder, position: Int) {
        holder.taskItem.tvTaskItemName.text = list[position].itemName
        holder.taskItem.itemCheckbox.isChecked = list[position].isCompleted
        if (list[position].isCompleted) {
            holder.taskItem.tvTaskItemName.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            holder.taskItem.tvTaskItemName.setTextColor(R.color.compliteColor)
        } else {
            holder.taskItem.tvTaskItemName.setTextColor(R.color.basicText)
        }
        holder.taskItem.itemCheckbox.setOnClickListener {
            onUpdateCheckbox(position, holder.taskItem.tvTaskItemName)
            activity.refreshTaskItemList()
        }
        holder.deleteItem.setOnClickListener {
            val dialog = AlertDialog.Builder(activity)
            dialog.setTitle("Удалить " + list[position].itemName)
            dialog.setMessage("Вы точно хотите удалить задачу?")
            dialog.setPositiveButton("Подтвердить") { _: DialogInterface, _: Int ->
                activity.dbHandler.deleteToItemTask(list[position].itemId)
                activity.refreshTaskItemList()
            }
            dialog.setNegativeButton("Отмена") { _: DialogInterface, _: Int ->
            }
            dialog.show()

        }
        holder.editItem.setOnClickListener {
            activity.updateItem(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("ResourceAsColor")
    private fun onUpdateCheckbox(position: Int, view: TextView) {
        list[position].isCompleted = !list[position].isCompleted
        if (list[position].isCompleted) {
            view.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            view.setTextColor(R.color.compliteColor)
        } else {
            view.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG.inv()
            view.setTextColor(R.color.basicText)
        }
        activity.dbHandler.updateItemTask(list[position])
    }

    class TaskItemHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = RvChildTaskItemBinding.bind(item)
        var taskItem = binding
        val editItem = binding.editBtn
        val deleteItem = binding.delBtn
    }
}