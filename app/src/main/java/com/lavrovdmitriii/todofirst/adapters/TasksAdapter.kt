package com.lavrovdmitriii.todofirst.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.lavrovdmitriii.todofirst.MainList
import com.lavrovdmitriii.todofirst.R
import com.lavrovdmitriii.todofirst.TaskItemActivity
import com.lavrovdmitriii.todofirst.constants.INTENT_TODO_ID
import com.lavrovdmitriii.todofirst.constants.INTENT_TODO_NAME
import com.lavrovdmitriii.todofirst.database.DBHandler
import com.lavrovdmitriii.todofirst.databinding.TasksRvChildBinding
import com.lavrovdmitriii.todofirst.dataclasses.TaskItem
import com.lavrovdmitriii.todofirst.dataclasses.Tasks

class TasksAdapter(private val activity: MainList, private val list: MutableList<Tasks>) : RecyclerView.Adapter<TasksAdapter.TasksHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TasksHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tasks_rv_child, parent, false)
        return TasksHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TasksHolder, position: Int) {
        amountTaskUpdate(position, holder)

        holder.taskName.setOnClickListener {
            val intent = Intent(activity, TaskItemActivity::class.java)
            intent.putExtra(INTENT_TODO_ID, list[position].id)
            intent.putExtra(INTENT_TODO_NAME, list[position].name)
            activity.startActivity(intent)
        }

        holder.menu.setOnClickListener {
            val popup = PopupMenu(activity, holder.menu)
            popup.inflate(R.menu.mainlist_child)
            popup.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.menuEdit -> {
                        activity.updateTaskList(list[position])
                    }
                    R.id.menuDelete -> {
                        activity.dbHandler.deleteToTask(list[position].id)
                        activity.refreshTaskList()
                    }
                    R.id.menuFinish -> {
                        activity.dbHandler.updateToTaskItemCompletedStatus(list[position].id, true)
                        amountTaskUpdate(position, holder)
                    }
                    R.id.menuReset -> {
                        activity.dbHandler.updateToTaskItemCompletedStatus(list[position].id, false)
                        amountTaskUpdate(position, holder)
                    }
                }
                true
            }
            popup.show()
        }
    }

    class TasksHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = TasksRvChildBinding.bind(item)
        val taskName = binding.tvTaskName
        val menu = binding.taskMenu
    }

    @SuppressLint("SetTextI18n")
    fun amountTaskUpdate (position: Int, holder: TasksHolder) {
        val itemsArr = activity.dbHandler.getToItemTask(list[position].id)
        val amountItems = itemsArr.size
        val amountNotChecked = arrayListOf<TaskItem>()
        for (item in 0 until amountItems) {
            if (!itemsArr[item].isCompleted) {
                amountNotChecked.add(itemsArr[item])
            }
        }
        holder.taskName.text = list[position].name + " (" + amountNotChecked.size + ")"
    }
}