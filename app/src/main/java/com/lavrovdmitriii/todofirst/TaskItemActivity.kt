package com.lavrovdmitriii.todofirst

import android.annotation.SuppressLint
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.lavrovdmitriii.todofirst.adapters.TaskItemAdapter
import com.lavrovdmitriii.todofirst.constants.INTENT_TODO_ID
import com.lavrovdmitriii.todofirst.constants.INTENT_TODO_NAME
import com.lavrovdmitriii.todofirst.database.DBHandler
import com.lavrovdmitriii.todofirst.databinding.ActivityTaskItemBinding
import com.lavrovdmitriii.todofirst.databinding.DialogTasksBinding
import com.lavrovdmitriii.todofirst.dataclasses.TaskItem

class TaskItemActivity : AppCompatActivity() {
    lateinit var dbHandler: DBHandler
    private lateinit var binding: ActivityTaskItemBinding
    private lateinit var bindingDialog: DialogTasksBinding
    var itemToTasksId: Long = -1
    @SuppressLint("ResourceAsColor", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTaskItemBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarList)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.title = "Назад"
        itemToTasksId = intent.getLongExtra(INTENT_TODO_ID, -1)

        dbHandler = DBHandler(this)
        binding.RWTaskItem.layoutManager = LinearLayoutManager(this)

        binding.parentTitle.text = intent.getStringExtra(INTENT_TODO_NAME)
        binding.amountTasks.setTextColor(R.color.black)

        binding.IWAddItemBtn.setOnClickListener {
            val dialog = AlertDialog.Builder(this)
            dialog.setTitle(intent.getStringExtra(INTENT_TODO_NAME) + ": добавить подзадачу")
            bindingDialog = DialogTasksBinding.inflate(layoutInflater)
            with(bindingDialog) {
                dialog.setView(bindingDialog.root)
                dialog.setPositiveButton("Добавить") { _: DialogInterface, _: Int ->
                    if (etTasks.text.isNotEmpty()) {
                        val itemTasks = TaskItem()
                        itemTasks.itemName = etTasks.text.toString()
                        itemTasks.itemToTasksId = itemToTasksId
                        dbHandler.addToItemTask(itemTasks)
                        refreshTaskItemList()
                    }
                }
                dialog.setNegativeButton("Отмена") { _: DialogInterface, _: Int ->
                }
                dialog.show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        refreshTaskItemList()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    fun updateItem(itemTasks: TaskItem) {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle(intent.getStringExtra(INTENT_TODO_NAME) + ": редактировать подзадачу")
        bindingDialog = DialogTasksBinding.inflate(layoutInflater)
        with(bindingDialog) {
            bindingDialog.etTasks.setText(itemTasks.itemName)
            dialog.setView(bindingDialog.root)
            dialog.setPositiveButton("Редактировать") { _: DialogInterface, _: Int ->
                if (etTasks.text.isNotEmpty()) {
                    itemTasks.itemName = etTasks.text.toString()
                    itemTasks.itemToTasksId = itemToTasksId
                    dbHandler.updateItemTask(itemTasks)
                    refreshTaskItemList()
                }
            }
            dialog.setNegativeButton("Отмена") { _: DialogInterface, _: Int ->
            }
            dialog.show()
        }
    }

    @SuppressLint("SetTextI18n")
    fun refreshTaskItemList() {
        binding.RWTaskItem.adapter = TaskItemAdapter(this, dbHandler.getToItemTask(itemToTasksId))
        val itemsArr = dbHandler.getToItemTask(itemToTasksId)
        val amountItems = itemsArr.size
        val amountChecked = arrayListOf<TaskItem>()
        for (item in 0 until amountItems) {
            if (itemsArr[item].isCompleted) {
                amountChecked.add(itemsArr[item])
            }
        }
        binding.amountTasks.text = amountChecked.size.toString() + "/" + amountItems.toString() + " выполнено"
    }

}