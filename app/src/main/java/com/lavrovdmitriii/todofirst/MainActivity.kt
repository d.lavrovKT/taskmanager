package com.lavrovdmitriii.todofirst

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.AnimationUtils
import com.lavrovdmitriii.todofirst.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding) {
            imageViewStartIcon.startAnimation(AnimationUtils.loadAnimation(this@MainActivity, R.anim.splash_in))
            Handler().postDelayed({
                imageViewStartIcon.startAnimation(AnimationUtils.loadAnimation(this@MainActivity, R.anim.splash_out))
                Handler().postDelayed({
                    startActivity(Intent(this@MainActivity, MainList::class.java))
                    finish()
                }, 500)
            }, 1500)
        }
    }
}